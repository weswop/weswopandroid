package com.iacopomiorini.weswop.models;

/**
 * Created by iacopomiorini on 01/03/15.
 */
public class WeSwopImage {

    public String id_image;
    public String id_item;
    public String image_path;

    public WeSwopImage(String image_path){
        this.image_path = image_path;
    }

    @Override
    public String toString() {
        return image_path;
    }
}
