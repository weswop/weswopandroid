package com.iacopomiorini.weswop.models;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class NegotiationDetail {
    public String id_negotiation;
    public String id_vendor;
    public String id_buyer;
    public String status;
    public String created;
    public NegotiationItem[] negotiation_items;

}
