package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class NegotationsResponse {

    @SerializedName("STATUS")
    public ResponseHeader status;

    @SerializedName("RESPONSE")
    public NegotiationDetail[] response;

}
