package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by iacopomiorini on 21/01/15.
 */
public class Item implements Serializable {
    public String id_item;
    public String id_fb;
    public String id_cat;
    public String id_subcat;
    @SerializedName("type")
    public String type_item;
    public String title;
    public String descr;
    public Float value;
    public Float geo_lat;
    public Float geo_lon;
    public String address;
    public String prov;
    public String postcode;
    public String city;
    public String insert_time;
    public String category_name;
    public String subcategory_name;
    public WeSwopImage[] images;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id: " + id_item + ",\n");
        sb.append("id_fb: " + id_fb + ",\n");
        sb.append("id_cat: " + id_cat + ",\n");
        sb.append("id_subcat: " + id_subcat + ",\n");
        sb.append("type_item: " + type_item + ",\n");
        sb.append("title: " + title + ",\n");
        sb.append("descr: " + descr + ",\n");
        sb.append("value: " + value + ",\n");
        sb.append("geo_lat: " + geo_lat + ",\n");
        sb.append("geo_lon: " + geo_lon + ",\n");
        sb.append("address: " + address + ",\n");
        sb.append("postcode: " + postcode + ",\n");
        sb.append("city: " + city + ",\n");
        sb.append("insert_time: " + insert_time + ",\n");
        sb.append("category_name: " + category_name + ",\n");
        sb.append("subcategory_name: " + subcategory_name + ",\n");
        sb.append("images: " + images + ",\n");
        if (images != null && images.length > 0) {
            sb.append("images: [\n");
            for (WeSwopImage image : images) {
                sb.append("   " + image.toString() + ",\n");
            }
            sb.append("]");
        }

        return sb.toString();
    }
}