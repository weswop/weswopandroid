package com.iacopomiorini.weswop.models;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class NegotiationItem {
    public String id_negotiation_item;
    public String id_negotiation;
    public String id_owner;
    public String id_item;
    public float money;
    public String added;
    public Item item_details;

}
