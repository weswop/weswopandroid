package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by iacopomiorini on 26/01/15.
 */
public class HomeRowResponse implements Serializable {

    @SerializedName("STATUS")
    public ResponseHeader status;

    @SerializedName("RESPONSE")
    public HomeRow[] response;


}
