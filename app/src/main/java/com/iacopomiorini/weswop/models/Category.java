package com.iacopomiorini.weswop.models;

import java.util.Date;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class Category {
    public String id_cat;
    public String name;
    public int catorder;
    public Date modtime;
    public Subcategory[] subcategories;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" [ ");
        if (subcategories != null) {
            for (Subcategory subCat : subcategories) {
                sb.append(subCat.name);
                sb.append(" - ");
            }
        }
        sb.append(" ] ");
        return sb.toString();
    }

    public class Subcategory {
        public String id_subcat;
        public String id_cat;
        public String name;
        public int subcatorder;
        public Date modtime;

    }
}
