package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class UserInfo {
    @SerializedName("id_fb")
    public String id;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    public String gender;
    public String locale;
    @SerializedName("fbname")
    public String fbName;
    @SerializedName("timezone")
    public String timeZone;
    @SerializedName("fbtoken")
    public String fbToken;
    @SerializedName("wwtoken")
    public String wwToken;
    @SerializedName("lastlogin")
    public Date lastLogin;

    @Override
    public String toString() {
        return "id: " + id + ", wwToken: " + wwToken + ", fbToken: " + fbToken;
    }
}
