package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class HomeRow{
    public int id;
    public String title;
    public String description;
    public int notification;
    public String data;
    public String hour;
    @SerializedName("imgUrl")
    public String imgUrl;
}
