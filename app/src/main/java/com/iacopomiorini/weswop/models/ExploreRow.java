package com.iacopomiorini.weswop.models;

import java.io.Serializable;

/**
 * Created by iacopomiorini on 20/01/15.
 */
public class ExploreRow implements Serializable {

    public int id;
    public String title;
    public String description;
    public float price;
    public String notification;
    public int category;
    public String imgUrl;

}
