package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iacopomiorini on 26/01/15.
 */
public class ResponseHeader {
    @SerializedName("CODE")
    public String code;
    @SerializedName("TEXT")
    public String text;
}
