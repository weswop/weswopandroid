package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by iacopomiorini on 26/01/15.
 */
public class CategoriesResponse implements Serializable{

    @SerializedName("STATUS")
    public ResponseHeader status;
    @SerializedName("RESPONSE")
    public Category[] response;


}
