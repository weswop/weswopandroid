package com.iacopomiorini.weswop.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iacopomiorini on 25/03/15.
 */
public class ItemsResponse {

    @SerializedName("STATUS")
    public ResponseHeader status;

    @SerializedName("RESPONSE")
    public Item[] response;

}
