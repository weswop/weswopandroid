package com.iacopomiorini.weswop.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class NegotiationRowBackground extends RelativeLayout {

    LayoutInflater mInflater;

    public NegotiationRowBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public NegotiationRowBackground(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public void init()
    {
        /*
        Creo viste, inietto layout

        mInflater.inflate(R.layout.custom_view, this, true);
        TextView tv = (TextView) v.findViewById(R.id.textView1);
        tv.setText(" Custom RelativeLayout");

        this.addView(tv);
        tv.setText(" Custom RelativeLayout");
        */
    }

    public void setupView(){
        //popolo viste, setto valori

    }
}
