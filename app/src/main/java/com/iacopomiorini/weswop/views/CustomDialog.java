package com.iacopomiorini.weswop.views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.activities.ItemDetailActivity;
import com.iacopomiorini.weswop.activities.NewItemActivity;
import com.iacopomiorini.weswop.managers.WeSwopApplication;

/**
 * Created by iacopomiorini on 01/03/15.
 */
public class CustomDialog extends Dialog {

    Context ctx;
    RelativeLayout newItemMenu, negotiationAddMenu, confirmNewObject, itemMenu;
    private NewItemActivity.OnDialogCallbackInterface listener;

    public CustomDialog(Context context, int theme) {
        super(context, theme);
        setContentView(R.layout.custom_dialog);

        ctx = context;

        newItemMenu = (RelativeLayout) findViewById(R.id.custom_dialog_new_item_menu);
        negotiationAddMenu = (RelativeLayout) findViewById(R.id.custom_dialog_negotiation_add_menu);
        confirmNewObject = (RelativeLayout) findViewById(R.id.custom_dialog_confirm_new_object);
        itemMenu = (RelativeLayout) findViewById(R.id.custom_dialog_item_menu);

    }

    public void setNewItemMenu() {
        newItemMenu.setVisibility(View.VISIBLE);

        LinearLayout cancel = (LinearLayout) findViewById(R.id.custom_dialog_layout_new_item_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        LinearLayout description = (LinearLayout) findViewById(R.id.custom_dialog_layout_new_item_description);
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent = new Intent(ctx, NewItemActivity.class);
                intent.putExtra(WeSwopApplication.NEW_OBJECT_MODE, WeSwopApplication.NEW_OBJECT_MODE_DESCRIPTION);
                ctx.startActivity(intent);
            }
        });

        LinearLayout photo = (LinearLayout) findViewById(R.id.custom_dialog_layout_new_item_photo);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent = new Intent(ctx, NewItemActivity.class);
                intent.putExtra(WeSwopApplication.NEW_OBJECT_MODE, WeSwopApplication.NEW_OBJECT_MODE_PHOTO);
                ctx.startActivity(intent);
            }
        });

        LinearLayout location = (LinearLayout) findViewById(R.id.custom_dialog_layout_new_item_location);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent = new Intent(ctx, NewItemActivity.class);
                intent.putExtra(WeSwopApplication.NEW_OBJECT_MODE, WeSwopApplication.NEW_OBJECT_MODE_LOCATION);
                ctx.startActivity(intent);
            }
        });
    }

    public void setNegotiationAddMenu(String name) {
        negotiationAddMenu.setVisibility(View.VISIBLE);

        TextView subtitle = (TextView) findViewById(R.id.custom_dialog_title_add_to_negotiation_subtitle);
        Resources res = getContext().getResources();
        subtitle.setText(String.format(res.getString(R.string.dialog_object_negotiation_oggetto_subtitle), name));

        LinearLayout cancel = (LinearLayout) findViewById(R.id.custom_dialog_layout_add_negotiation_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    public void setItemMenu(final ItemDetailActivity.OnDialogCallbackInterface listener) {
        itemMenu.setVisibility(View.VISIBLE);

        Button newNegotiation = (Button) findViewById(R.id.btn_customdialog_item_menu_new_negotiation);
        newNegotiation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNewNegotiationClick();
            }
        });

        Button existingNegotiation = (Button) findViewById(R.id.btn_customdialog_item_menu_exists);
        existingNegotiation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onExistingNegotiationClick();
            }
        });

        Button preferred = (Button) findViewById(R.id.btn_customdialog_item_menu_preferred);
        preferred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPreferredClick();
            }
        });

    }

    public void setConfirmNewObject(final NewItemActivity.OnDialogCallbackInterface listener) {
        this.listener = listener;
        confirmNewObject.setVisibility(View.VISIBLE);

        LinearLayout cancel = (LinearLayout) findViewById(R.id.ll_customdialog_newobjectconfim_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button add = (Button) findViewById(R.id.btn_customdialog_newobjectconfim_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDialogCallback();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void show() {
        super.show();
    }

}
