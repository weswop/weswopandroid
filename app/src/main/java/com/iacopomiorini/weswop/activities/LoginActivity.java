package com.iacopomiorini.weswop.activities;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.iacopomiorini.weswop.activities.util.SystemUiHider;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;

import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.fragments.FacebookFragment;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.CategoriesResponse;
import com.iacopomiorini.weswop.models.UserInfo;
import com.iacopomiorini.weswop.models.UserInfoResponse;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;

import static com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable;
import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class LoginActivity extends FragmentActivity implements FacebookFragment.onLoginFacebookListener {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    private final static long splashTime = 4000;
    long startTime = 0;
    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */

    private FacebookFragment facebookFragment;
    private boolean loggedIn = false;
    private AQuery $;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        $ = new AQuery(this);

        if (savedInstanceState == null) {
            // Add the fragment on initial activity setup
            facebookFragment = new FacebookFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, facebookFragment)
                    .commit();

        } else {

            // Or set the fragment from restored state info
            facebookFragment = (FacebookFragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        startTime = SystemClock.elapsedRealtime();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isGooglePlayServicesAvailable(this);
    }

    public void goToMainOrPreview() {

        if (loggedIn) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
        } else {
            $.id(R.id.loader_layout).invisible();
        }
    }

    @Override
    public void onLogin(final Session session) {
        if (!loggedIn) {
            loggedIn = true;
            Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    weSwopLogin(session, user);
                }
            });
            Request.executeBatchAsync(request);
            WeSwopLog("Login Activity logged " + session.getApplicationId());
        }
    }

    @Override
    public void onLogout() {
        WeSwopLog("Login Activity logged out");
    }

    private void weSwopLogin(Session session, GraphUser user){

        if (session == Session.getActiveSession()) {
            if (user != null) {
                String strRegistrazione = getResources().getString(R.string.URL_REGISTRATION_FB_TOKEN);
                String urlRegistrazione = String.format(strRegistrazione, session.getAccessToken(), user.getId());

                $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlRegistrazione, UserInfoResponse.class, new AjaxCallback<UserInfoResponse>() {
                    @Override
                    public void callback(String url, UserInfoResponse result, AjaxStatus status) {
                        super.callback(url, result, status);

                        if (status.getCode() == 200) {

                            WeSwopLog("User Info result " + result.toString());
                            if (result.response != null) {
                                getCategories(result);
                            }

                        }else{
                            WeSwopApplication.toast("WeSwop login Error");
                            WeSwopLog("WeSwop login Error " + status.getCode());
                        }
                    }

                    @Override
                    public void failure(int code, String message) {
                        super.failure(code, message);
                        WeSwopApplication.toast("Facebook login Error");
                        WeSwopLog("User Info Error " + code + " - " + message.toString());
                    }
                });
            }
        }
    }

    private void getCategories(UserInfoResponse result){

        WeSwopApplication.getInstance().setUserInfo(result.response);

        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();

        String strCategories = getResources().getString(R.string.URL_GET_CATEGORIES);
        String urlCategories = String.format(strCategories, userInfo.id, userInfo.wwToken, Locale.getDefault().getLanguage());

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlCategories, CategoriesResponse.class, new AjaxCallback<CategoriesResponse>() {
            @Override
            public void callback(String url, CategoriesResponse result, AjaxStatus status) {
                super.callback(url, result, status);

                if (status.getCode() == 200) {
                    WeSwopApplication.getInstance().setCategories(result.response);
                    WeSwopLog("Categories " + Arrays.toString(WeSwopApplication.getInstance().getCategories()));
                    long endTime = SystemClock.elapsedRealtime();
                    final long elapsedMilliSeconds = endTime - startTime;
                    if (elapsedMilliSeconds <= splashTime) {

                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    synchronized (this) {
                                        wait(splashTime - elapsedMilliSeconds);
                                        goToMainOrPreview();
                                    }
                                } catch (InterruptedException ex) {

                                }
                            }
                        };
                        thread.start();

                    } else {
                        goToMainOrPreview();
                    }
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                WeSwopApplication.toast("Errore di connessione al server");
                WeSwopLog("Get categories rrror " + code + " - " + message.toString());
            }
        });
    }
}
