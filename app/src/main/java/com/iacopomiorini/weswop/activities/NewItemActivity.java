package com.iacopomiorini.weswop.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.helpers.AlbumStorageDirFactory;
import com.iacopomiorini.weswop.helpers.BaseAlbumDirFactory;
import com.iacopomiorini.weswop.helpers.FroyoAlbumDirFactory;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Category;
import com.iacopomiorini.weswop.models.UserInfo;
import com.iacopomiorini.weswop.models.WeSwopImageResponse;
import com.iacopomiorini.weswop.models.ItemResponse;
import com.iacopomiorini.weswop.utilities.Util;
import com.iacopomiorini.weswop.views.CustomDialog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopError;
import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class NewItemActivity extends BaseActivity implements View.OnLongClickListener {

    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private static final int IMAGE_NUMBER = 6;
    RelativeLayout elementCategory;
    LinearLayout photosFirstRow, photosSecondRow, photosThirdRow;
    TextView tvCategoryName, tvSubcategoryName, tvPositionTitle, tvPositionDescription;
    FormEditText etTitle, etDescription, etPrice;
    String selectedCategory = "", selectedSubcategory = "";
    int nextImageIndex = 0;
    Boolean hasCamera = true;
    String[] imagesPath = new String[IMAGE_NUMBER];
    ImageView[] imagesView = new ImageView[IMAGE_NUMBER];
    Place address;
    FormEditText[] allFields = new FormEditText[3];
    private String mCurrentPhotoPath;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private int compressionRatio = 50;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_new_object;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tvPositionDescription = $.id(R.id.tv_label_new_object_position_description).getTextView();
        tvPositionTitle = $.id(R.id.tv_label_new_object_position_title).getTextView();

        tvCategoryName = $.id(R.id.tv_category_name).getTextView();
        tvSubcategoryName = $.id(R.id.tv_subcategory_name).getTextView();
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            imagesPath[i] = "";
        }
        imagesView[0] = $.id(R.id.test_photo).getImageView();
        imagesView[1] = $.id(R.id.test_photo2).getImageView();
        imagesView[2] = $.id(R.id.test_photo3).getImageView();
        imagesView[3] = $.id(R.id.test_photo4).getImageView();
        imagesView[4] = $.id(R.id.test_photo5).getImageView();
        imagesView[5] = $.id(R.id.test_photo6).getImageView();

        imagesView[0].setOnLongClickListener(this);
        imagesView[1].setOnLongClickListener(this);
        imagesView[2].setOnLongClickListener(this);
        imagesView[3].setOnLongClickListener(this);
        imagesView[4].setOnLongClickListener(this);
        imagesView[5].setOnLongClickListener(this);

        photosFirstRow = (LinearLayout) $.id(R.id.ll_photos_first_row).getView();
        photosSecondRow = (LinearLayout) $.id(R.id.ll_photos_second_row).getView();
        photosThirdRow = (LinearLayout) $.id(R.id.ll_photos_third_row).getView();

        etTitle = (FormEditText) $.id(R.id.et_title).getView();
        etDescription = (FormEditText) $.id(R.id.et_description).getView();
        etPrice = (FormEditText) $.id(R.id.et_price).getView();

        allFields[0] = etTitle;
        allFields[1] = etDescription;
        allFields[2] = etPrice;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

        PackageManager packageManager = NewItemActivity.this.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false) {
            Toast.makeText(NewItemActivity.this, "This device does not have a camera.", Toast.LENGTH_SHORT).show();
            hasCamera = false;
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString(WeSwopApplication.NEW_OBJECT_MODE);
            if (value.equals(WeSwopApplication.NEW_OBJECT_MODE_PHOTO)) {
                WeSwopLog("New object take photo");
                if (hasCamera) {
                    openImageIntent();
                } else {
                    Toast.makeText(NewItemActivity.this, "This device does not have a camera.", Toast.LENGTH_SHORT).show();
                }
            } else if (value.equals(WeSwopApplication.NEW_OBJECT_MODE_LOCATION)) {
                WeSwopLog("New object take location");
                openPlacePicker(new View(this));
            } else if (value.equals(WeSwopApplication.NEW_OBJECT_MODE_DESCRIPTION)) {
                WeSwopLog("New object take description");
            }
        }

        $.id(R.id.element_category).clicked(this, "openCategorySelection");
        $.id(R.id.fb_create_object).clicked(this, "createObject");
        $.id(R.id.rl_newobject_insertphoto).clicked(this, "openImageIntent");
        $.id(R.id.rl_newobject_placepicker).clicked(this, "openPlacePicker");

    }

    public void postItem() {
        String url = getString(R.string.URL_POST_ITEM);
        String addressString = "", city = "", postcode = "", prov = "";
        String[] addressSplit;
        addressSplit = address.getAddress().toString().split(",");
        if (addressSplit.length > 1) {
            String[] addressSplitSpace = addressSplit[2].split(" ");
            if (addressSplitSpace.length > 1) {
                postcode = addressSplitSpace[1];
                WeSwopLog(postcode);
                addressString = addressSplit[0] + "," + addressSplit[1];
            }
            if (addressSplitSpace.length > 2) {
                city = addressSplitSpace[2];
                WeSwopLog(city);
            }
            if (addressSplitSpace.length > 3) {
                prov = addressSplitSpace[3];
                WeSwopLog(prov);
            }

        }
        Map<String, Object> params = new HashMap<>();
        params.put("id_fb", WeSwopApplication.getInstance().getUserInfo().id);
        params.put("wwtoken", WeSwopApplication.getInstance().getUserInfo().wwToken);
        params.put("id_cat", selectedCategory);
        params.put("id_subcat", selectedSubcategory);
        params.put("type", "Object");
        params.put("title", etTitle.getText());
        params.put("descr", etDescription.getText());
        params.put("value", etPrice.getText().toString());
        params.put("geo_lat", address.getLatLng().latitude);
        params.put("geo_lon", address.getLatLng().longitude);
        params.put("address", addressString);
        params.put("prov", prov);
        params.put("postcode", postcode);
        params.put("city", city);

        //WeSwopLog(params.toString());

        dialog.setMessage(getResources().getString(R.string.loader_creating_object));
        $.transformer(GSONHelper.getInstance().getTransformer()).progress(dialog).ajax(url, params, ItemResponse.class, new AjaxCallback<ItemResponse>() {

            @Override
            public void callback(String url, ItemResponse item, AjaxStatus status) {
                super.callback(url, result, status);

                if (status.getCode() == 200) {
                    if (getImagesLength() > 0) {
                        postImages(result.response.id_item);
                    } else {
                        WeSwopApplication.toast("New item created");
                        finish();
                    }

                } else {
                    WeSwopApplication.toast("Error creating new item");
                    WeSwopError("Error post item");
                }
            }
        });

    }

    private void postImages(String idItem) {
        WeSwopLog("Images number " + getImagesLength());

        new UploadTask(idItem).execute(imagesPath);

        /*
        for (int i = 0; i < getImagesLength(); i++) {
            uploadImage(i, idItem);
        }
        */
    }

    public void openCategorySelection(View v) {
        Intent i = new Intent(NewItemActivity.this, CategoriesActivity.class);
        startActivityForResult(i, WeSwopApplication.REQUEST_CODE_CATEGORY);
    }

    public void createObject(View v) {

        //Controlli presenza campi
        boolean allValid = true;

        if (address == null || address.getAddress() == null) {
            WeSwopApplication.toast("Inserisci una posizione");
            allValid = false;
        }
        if (selectedCategory.equals("") || selectedSubcategory.equals("")) {
            WeSwopApplication.toast("Seleziona categoria e sottocategoria");
            allValid = false;
        }
        for (FormEditText field : allFields) {
            allValid = field.testValidity() && allValid;
        }

        if (!allValid) {
            return;
        }

        final CustomDialog dialog = new CustomDialog(NewItemActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setConfirmNewObject(new OnDialogCallbackInterface() {
            @Override
            public void onDialogCallback() {
                dialog.dismiss();
                postItem();
            }
        });
        dialog.show();
    }

    public void openImageIntent(View v) {
        openImageIntent();
    }

    public void openPlacePicker(View v) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, WeSwopApplication.PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            Log.e("TESTING", e.toString());
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("TESTING", e.toString());
        }
    }

    private String getAlbumName() {
        return getString(R.string.album_name);
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        WeSwopLog("failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            WeSwopLog("External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File setUpPhotoFile() throws IOException {
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        return f;
    }

    private void setPic(String currentImagePath) {

		/* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView*/
        int targetW = imagesView[0].getWidth();
        int targetH = imagesView[0].getHeight();

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less*/
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		/* Set bitmap options to scale the image decode target*/
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        Bitmap b = Util.rotateBitmap(mCurrentPhotoPath, bitmap);

		/* Associate the Bitmap to the ImageView */
        int currentIndex = getNextImageIndex();
        imagesView[currentIndex].setImageBitmap(b);
        imagesPath[currentIndex] = currentImagePath;
        if (currentIndex < 2) {
            photosFirstRow.setVisibility(View.VISIBLE);
        } else if (currentIndex < 4) {
            photosSecondRow.setVisibility(View.VISIBLE);
        } else if (currentIndex < 6) {
            photosThirdRow.setVisibility(View.VISIBLE);
        }
        $.id(R.id.rl_newobject_insertphoto).height(LinearLayout.LayoutParams.WRAP_CONTENT);
        $.id(R.id.iv_photos_arrow).gone();
    }

    private void openImageIntent() {

        // Determine Uri of camera image to save.
        /*
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "WeSwop_Photos" + File.separator);
        root.mkdirs();
        final String fname = "img_"+ System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);
        */

        File f = null;
        try {
            f = setUpPhotoFile();
            mCurrentPhotoPath = f.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            cameraIntents.add(intent);
        }

        // Filesystem.
        /*
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        */
        final Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

        startActivityForResult(chooserIntent, WeSwopApplication.YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == WeSwopApplication.REQUEST_CODE_CATEGORY) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result");
                //WeSwopLog("Return category "+result);
                selectedCategory = result.split("_")[0];
                selectedSubcategory = result.split("_")[1];
                Category category = WeSwopApplication.getInstance().getCategorySubcategoryById(selectedCategory, selectedSubcategory);
                if (category != null && category.name != null && category.subcategories != null && category.subcategories.length > 0) {
                    WeSwopLog("Ehi category selected is " + category.toString());
                    tvCategoryName.setText(category.name);
                    tvSubcategoryName.setText(category.subcategories[0].name);
                }
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == WeSwopApplication.PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                //String toastMsg = String.format("Place: %s", place.getAddress());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                address = place;
                WeSwopLog(place.getAddress().toString());
                tvPositionTitle.setVisibility(View.GONE);
                tvPositionDescription.setText(address.getAddress().toString());
                tvPositionDescription.setGravity(Gravity.CENTER);
            }
        } else if (requestCode == WeSwopApplication.YOUR_SELECT_PICTURE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    WeSwopLog("From Gallery");
                    mCurrentPhotoPath = Util.getRealPathFromURI(NewItemActivity.this, data.getData());
                }

                if (mCurrentPhotoPath != null) {
                    WeSwopLog("Photo " + mCurrentPhotoPath);
                    setPic(mCurrentPhotoPath);
                    galleryAddPic();
                    printImages();
                    mCurrentPhotoPath = null;
                }
            }
        }
    }

    private int getNextImageIndex() {
        int next = -1;
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            if (imagesPath[i] == "") {
                return i;
            }
        }
        return -1;
    }

    private int getImagesLength() {
        int number = 0;
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            if (!imagesPath[i].equals("")) {
                number++;
            } else {
                return number;
            }
        }
        return number;
    }

    private int getIndexById(View v) {
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            if (v.getId() == imagesView[i].getId()) {
                return i;
            }
        }
        return -1;
    }

    private void removeImage(int i) {
        for (int j = i; j < IMAGE_NUMBER; j++) {
            imagesPath[i] = "";
            if (imagesPath[i + 1] != "") {
                setPic(imagesPath[i + 1]);
                imagesPath[i] = imagesPath[i + 1];
            }
        }
    }

    private void compressImages() {
        printImages();
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            if (imagesPath[i] == "" && imagesPath[i + 1] != "") {
                for (int j = i; i < IMAGE_NUMBER - 1; i++) {
                    imagesPath[i] = imagesPath[i + 1];
                }
                imagesPath[IMAGE_NUMBER - 1] = "";
                printImages();
                break;
            }
        }
    }

    private void printImages() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < IMAGE_NUMBER; i++) {
            sb.append(imagesPath[i] + ", ");
        }
        sb.append("]");
        WeSwopLog(sb.toString());
    }

    @Override
    public boolean onLongClick(View v) {
        removeImage(getIndexById(v));
        return false;
    }

    private class UploadTask extends AsyncTask<String, Integer, Long> {

        private String id_item;
        private String urlPost;
        private HttpClient httpclient;
        private HttpPost httppost;
        private UserInfo userInfo;

        public UploadTask(String id_item) {
            this.id_item = id_item;
            urlPost = getResources().getString(R.string.URL_POST_IMAGE);
            httpclient = new DefaultHttpClient();
            httppost = new HttpPost(urlPost);
            userInfo = WeSwopApplication.getInstance().getUserInfo();
        }

        protected Long doInBackground(String... paths) {

            for (int i = 0; i < getImagesLength(); i++) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    WeSwopLog("Path image -> " + paths[i]);
                    Bitmap bitmap = BitmapFactory.decodeFile(paths[i], options);
                    bitmap = Util.rotateBitmap(paths[i], bitmap);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, compressionRatio, stream);

                    MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                    File file = new File(paths[i]);
                    reqEntity.addPart(paths[i], new FileBody(file, "image/jpeg"));
                    reqEntity.addPart("id_item", new StringBody(id_item));
                    reqEntity.addPart("id_fb", new StringBody(userInfo.id));
                    reqEntity.addPart("wwtoken", new StringBody(userInfo.wwToken));

                    httppost.setEntity(reqEntity);
                    httppost.setHeader("Content-Type", "multipart/form-data");

                    HttpResponse response = httpclient.execute(httppost);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();

                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    System.out.println("Response: " + s);
                } catch (Exception e) {
                    WeSwopError(e.getMessage());
                }
            }

            return -1l;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        protected void onPostExecute(Long result) {
            dialog.dismiss();
            if (result == 200) {

            } else {

            }
        }
    }

    private void uploadImage(final int index, String idItem) {
        String number = getResources().getString(R.string.loader_upload_photo_number);
        String of = getResources().getString(R.string.loader_upload_photo_of);
        dialog.setMessage(number + " " + (index + 1) + " " + of + " " + getImagesLength());
        dialog.show();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        WeSwopLog("Path image " + index + " -> " + imagesPath[index]);
        Bitmap bitmap = BitmapFactory.decodeFile(imagesPath[index], options);
        bitmap = Util.rotateBitmap(imagesPath[index], bitmap);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, compressionRatio, stream);

        String urlPost = getResources().getString(R.string.URL_POST_IMAGE);
        Map<String, Object> params = new HashMap<>();
        params.put("id_fb", WeSwopApplication.getInstance().getUserInfo().id);
        params.put("wwtoken", WeSwopApplication.getInstance().getUserInfo().wwToken);
        params.put("id_item", idItem);
        params.put("file_name", imagesPath[index]);
        params.put("source", stream.toByteArray());
        WeSwopLog("Posting image for object " + idItem);
        WeSwopLog("Posting image params " + params.toString());

        AjaxCallback<WeSwopImageResponse> cb = new AjaxCallback<WeSwopImageResponse>() {

            @Override
            public void callback(String url, WeSwopImageResponse response, AjaxStatus status) {
                super.callback(url, result, status);
                dialog.dismiss();
                if (status.getCode() == 200) {
                    WeSwopLog("Upload success photo!");
                    if (index == getImagesLength() - 1) {
                        WeSwopApplication.toast("Item created");
                        finish();
                    }
                } else {
                    WeSwopApplication.toast("Item created, Upload photo error");
                    WeSwopError("Upload photo error " + this.status.getCode());
                    finish();
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                dialog.dismiss();
                WeSwopApplication.toast("New item created but upload photo error");
                WeSwopError("Upload photo error " + code + " " + message);
            }
        };
        cb.header("Content-Type", "image/jpeg; charset=utf-8");

        $.ajax(urlPost, params, WeSwopImageResponse.class, cb);
    }

    public interface OnDialogCallbackInterface {
        void onDialogCallback();
    }

}
