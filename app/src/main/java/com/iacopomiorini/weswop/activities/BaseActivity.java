package com.iacopomiorini.weswop.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.androidquery.AQuery;
import com.iacopomiorini.weswop.R;

/**
 * Created by iacopomiorini on 27/09/15.
 */
public abstract class BaseActivity extends Activity {

    protected AQuery $;
    protected ProgressDialog dialog;
    protected ActionBar actionBar;

    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        $ = new AQuery(this);

        actionBar = getActionBar();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.loading);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
