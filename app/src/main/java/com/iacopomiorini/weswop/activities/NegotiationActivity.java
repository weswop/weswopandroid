package com.iacopomiorini.weswop.activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Activity;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.NegotationDetailResponse;
import com.iacopomiorini.weswop.models.NegotiationDetail;
import com.iacopomiorini.weswop.models.NegotiationItem;
import com.iacopomiorini.weswop.models.UserInfo;

import java.util.HashMap;
import java.util.Map;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopError;

public class NegotiationActivity extends BaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_negotiation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String negotiationId = getIntent().getStringExtra(WeSwopApplication.NEGOTIATION_DETAILS);
        getNegotiationDetails(negotiationId);

        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    private void getNegotiationDetails(String negotiationId){

        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();

        String url = getString(R.string.URL_GET_NEGOTIATION_DETAIL);
        String urlFormat = String.format(url, userInfo.id, userInfo.wwToken, negotiationId);

        $.transformer(GSONHelper.getInstance().getTransformer()).progress(dialog).ajax(urlFormat, NegotationDetailResponse.class, new AjaxCallback<NegotationDetailResponse>() {

            @Override
            public void callback(String url, NegotationDetailResponse item, AjaxStatus status) {
                super.callback(url, result, status);

                if (status.getCode() == 200) {
                    setupView(item.response);
                } else {
                    WeSwopApplication.toast("Error retrieving negotiation details");
                    WeSwopError("Error post negotiation");
                    finish();
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
            }

        });

    }

    private void setupView(NegotiationDetail negotiation){

    }

}
