package com.iacopomiorini.weswop.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AppEventsLogger;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.fragments.ExploreFragment;
import com.iacopomiorini.weswop.fragments.MainFragment;
import com.iacopomiorini.weswop.fragments.MessagesFragment;
import com.iacopomiorini.weswop.fragments.MyItemsFragment;
import com.iacopomiorini.weswop.fragments.NegotiationsFragment;
import com.iacopomiorini.weswop.fragments.PlaceholderFragment;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Item;
import com.iacopomiorini.weswop.models.NegotiationDetail;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Locale;


public class MainActivity extends BaseActivity implements
        MainFragment.OnFragmentInteractionListener,
        ExploreFragment.OnFragmentInteractionListener,
        NegotiationsFragment.OnFragmentInteractionListener,
        MyItemsFragment.OnFragmentInteractionListener,
        MessagesFragment.OnFragmentInteractionListener {

    SectionsPagerAdapter homePagerAdapter;
    ViewPager homeViewPager;
    CirclePageIndicator cpi;
    View homeActionBar;
    private int previousState, currentState;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);

        homePagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        homeViewPager = (ViewPager) findViewById(R.id.activity_main_pager);
        homeViewPager.setAdapter(homePagerAdapter);

        homeActionBar = getLayoutInflater().inflate(R.layout.action_bar_custom, null);
        cpi = (CirclePageIndicator) homeActionBar.findViewById(R.id.actionBar_circlePageIndicator);
        final TextView sectionTitle = (TextView) homeActionBar.findViewById(R.id.tv_actionbar_section);
        cpi.setViewPager(homeViewPager);
        actionBar.setCustomView(homeActionBar);
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        sectionTitle.setText(homePagerAdapter.getPageTitle(0));

        cpi.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                sectionTitle.setText(homePagerAdapter.getPageTitle(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int currentPage = homeViewPager.getCurrentItem();       //ViewPager Type

                if (currentPage == homePagerAdapter.getCount() - 1 || currentPage == 0) {
                    previousState = currentState;
                    currentState = state;
                    if (previousState == 1 && currentState == 0) {

                        homeViewPager.setCurrentItem(currentPage == 0 ? homePagerAdapter.getCount() - 1 : 0);

                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
        //super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(int page) {
        homeViewPager.setCurrentItem(WeSwopApplication.idsPages.get(page));
    }

    public void goToMessaggi(View v) {
        homeViewPager.setCurrentItem(WeSwopApplication.PAGE_NOTIFICHE_MESSAGGI);
    }

    public void goToMieiOggetti(View v) {
        homeViewPager.setCurrentItem(WeSwopApplication.PAGE_MIEI_OGGETTI);
    }

    public void goToTrattative(View v) {
        homeViewPager.setCurrentItem(WeSwopApplication.PAGE_TRATTATIVE);
    }

    public void goToOggettiAmici(View v) {
        homeViewPager.setCurrentItem(WeSwopApplication.PAGE_ESPLORA);
    }

    public void goToTuttiOggetti(View v) {
        homeViewPager.setCurrentItem(WeSwopApplication.PAGE_ESPLORA);
    }

    @Override
    public void openObjectDetail(Item item) {
        Intent i = new Intent(this, ItemDetailActivity.class);
        i.putExtra(WeSwopApplication.OBJECT_DETAILS, GSONHelper.getInstance().encode(item));
        startActivity(i);
    }

    @Override
    public void openNegotiationDetail(NegotiationDetail negotiation) {
        //vado al dettaglio trattativa
        Intent i = new Intent(this, NegotiationActivity.class);
        i.putExtra(WeSwopApplication.NEGOTIATION_DETAILS, negotiation.id_negotiation);
        startActivity(i);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new MainFragment();
                case 1:
                    return new ExploreFragment();
                case 2:
                    return new NegotiationsFragment();
                case 3:
                    return new MyItemsFragment();
                case 4:
                    return new MessagesFragment();
                default:
                    return PlaceholderFragment.newInstance("param1", "param2");
            }
        }

        @Override
        public int getCount() {
            return WeSwopApplication.PAGES_NUMBER;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1);
                case 1:
                    return getString(R.string.title_section2);
                case 2:
                    return getString(R.string.title_section3);
                case 3:
                    return getString(R.string.title_section4);
                case 4:
                    return getString(R.string.title_section5);
            }
            return null;
        }
    }
}
