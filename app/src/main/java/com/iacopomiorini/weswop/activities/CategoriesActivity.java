package com.iacopomiorini.weswop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.fragments.CategoriesFragment;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class CategoriesActivity extends BaseActivity implements CategoriesFragment.OnFragmentInteractionListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFragmentInteraction(String name) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", name);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_categories;
    }
}
