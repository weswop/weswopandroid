package com.iacopomiorini.weswop.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.adapters.ImagePagerAdapter;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Category;
import com.iacopomiorini.weswop.models.Item;
import com.iacopomiorini.weswop.models.NegotationDetailResponse;
import com.iacopomiorini.weswop.models.NegotationItemResponse;
import com.iacopomiorini.weswop.models.UserInfo;
import com.iacopomiorini.weswop.models.UserInfoResponse;
import com.iacopomiorini.weswop.models.WeSwopImage;
import com.iacopomiorini.weswop.utilities.Util;
import com.iacopomiorini.weswop.views.CircleImageView;
import com.iacopomiorini.weswop.views.CustomDialog;
import com.viewpagerindicator.CirclePageIndicator;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopError;
import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

/**
 * Created by iacopomiorini on 20/01/15.
 */
public class ItemDetailActivity extends BaseActivity {

    private CirclePageIndicator cpi;
    private ImagePagerAdapter ipa;
    private ViewPager imagesViewPager;
    private RelativeLayout imagesViewPagerContent;
    private CircleImageView userCircleImageView;
    private TextView userNameTextView;
    private TextView categoryTextView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private TextView priceTextView;
    private Item objectDetails;
    private FloatingActionButton addNegotiation;
    private String idVendor = "";
    private AdView mAdView;
    private UserInfo userInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_object_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String objectDetailsString = getIntent().getStringExtra(WeSwopApplication.OBJECT_DETAILS);
        objectDetails = GSONHelper.getInstance().decode(objectDetailsString, Item.class);

        WeSwopLog("Open object :\n" + objectDetails.toString() + "\n--------------");

        actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        imagesViewPager = (ViewPager) findViewById(R.id.objectDetail_pager);
        imagesViewPagerContent = (RelativeLayout) findViewById(R.id.objectDetail_pager_content);
        userCircleImageView = (CircleImageView) findViewById(R.id.objectDetail_circleimageview);
        cpi = (CirclePageIndicator) findViewById(R.id.objectDetail_circlePageIndicator);
        userNameTextView = (TextView) findViewById(R.id.objectDetail_userName);
        categoryTextView = (TextView) findViewById(R.id.tv_category);
        priceTextView = (TextView) findViewById(R.id.tv_price);
        titleTextView = (TextView) findViewById(R.id.tv_title);
        descriptionTextView = (TextView) findViewById(R.id.tv_description);
        addNegotiation = (FloatingActionButton) findViewById(R.id.fab_add_to_negotiation);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        userInfo = WeSwopApplication.getInstance().getUserInfo();

        setupView();
    }

    private void setupView() {
        String strCategories = getResources().getString(R.string.URL_GET_USER_INFO);
        String urlCategories = String.format(strCategories, userInfo.id, userInfo.wwToken, objectDetails.id_fb);

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlCategories, UserInfoResponse.class, new AjaxCallback<UserInfoResponse>() {
            @Override
            public void callback(String url, final UserInfoResponse result, AjaxStatus status) {
                super.callback(url, result, status);

                if (status.getCode() == 200) {
                    if (result.response != null) {
                        userNameTextView.setText(result.response.fbName);
                        idVendor = result.response.id;
                        if (!idVendor.equals("")) {
                            URL urlImg = null;
                            try {
                                urlImg = new URL("https://graph.facebook.com/" + idVendor + "/picture?type=large");
                                userCircleImageView.setImageUrl(urlImg.toString(), WeSwopApplication.getInstance().getImageLoader());
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                        addNegotiation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(getActivity(), "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                                final CustomDialog dialog = new CustomDialog(ItemDetailActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
                                dialog.setItemMenu(new OnDialogCallbackInterface() {

                                    @Override
                                    public void onNewNegotiationClick() {
                                        dialog.dismiss();
                                        postNewNegotiation();
                                    }

                                    @Override
                                    public void onExistingNegotiationClick() {
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onPreferredClick() {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        });
                    }
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
            }
        });

        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        int height = (Util.getScreenHeight(this) - actionBarHeight) / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imagesViewPager.getLayoutParams();
        params.height = height;
        imagesViewPager.setLayoutParams(params);
        LinearLayout.LayoutParams paramsContent = (LinearLayout.LayoutParams) imagesViewPagerContent.getLayoutParams();
        paramsContent.height = height;
        imagesViewPagerContent.setLayoutParams(paramsContent);
        RelativeLayout.LayoutParams params3 = (RelativeLayout.LayoutParams) userCircleImageView.getLayoutParams();
        params3.setMargins(0, height - params3.height / 2, Util.convertDpToPixel(10, this), 0);
        userCircleImageView.setLayoutParams(params3);

        if (objectDetails.images.length == 0) {
            cpi.setVisibility(View.GONE);
            objectDetails.images = new WeSwopImage[1];
            objectDetails.images[0] = new WeSwopImage(getResources().getString(R.string.PLACEHOLDER_PATH));
        } else if (objectDetails.images.length == 1) {
            cpi.setVisibility(View.GONE);
        }
        ipa = new ImagePagerAdapter(ItemDetailActivity.this, objectDetails.images, WeSwopApplication.getInstance().getImageLoader());
        imagesViewPager.setAdapter(ipa);
        cpi.setViewPager(imagesViewPager);

        titleTextView.setText(objectDetails.title);
        priceTextView.setText(objectDetails.value + "€");
        descriptionTextView.setText(objectDetails.descr);
        Category category = WeSwopApplication.getInstance().getCategorySubcategoryById(objectDetails.id_cat, objectDetails.id_subcat);
        if (category != null && category.name != null && category.subcategories.length > 0) {
            categoryTextView.setText(category.name + " - " + category.subcategories[0].name);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void postNewNegotiation() {
        String url = getString(R.string.URL_POST_NEGOTIATION);

        Map<String, Object> params = new HashMap<>();
        params.put("id_fb", userInfo.id);
        params.put("wwtoken", userInfo.wwToken);
        params.put("id_vendor", idVendor);
        params.put("id_buyer", userInfo.id);
        params.put("status", "new");

        dialog.setMessage(getResources().getString(R.string.loader_new_negotiation));
        $.transformer(GSONHelper.getInstance().getTransformer()).progress(dialog).ajax(url, params, NegotationDetailResponse.class, new AjaxCallback<NegotationDetailResponse>() {

            @Override
            public void callback(String url, NegotationDetailResponse item, AjaxStatus status) {
                super.callback(url, result, status);
                //WeSwopLog("Status code " + status.getCode());

                if (status.getCode() == 200) {
                    //aggiungo alla trattativa item voluto
                    postFirstItem(result.response.id_negotiation, result.response.id_vendor);

                } else {
                    WeSwopApplication.toast("Errore nella creazione della trattativa");
                    WeSwopError("Error creating negotiation " + status.getCode());
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
            }

        });
    }

    private void postFirstItem(String idNegotiation, String idVendor) {
        Map<String, Object> params = new HashMap<>();
        params.put("id_fb", userInfo.id);
        params.put("wwtoken", userInfo.wwToken);
        params.put("id_negotiation", idNegotiation);
        params.put("id_owner", idVendor);
        params.put("id_item", objectDetails.id_item);
        params.put("money", 0);

        String url = getString(R.string.URL_POST_NEGOTIATION_ITEM);
        $.transformer(GSONHelper.getInstance().getTransformer()).progress(dialog).ajax(url, params, NegotationItemResponse.class, new AjaxCallback<NegotationItemResponse>() {

            @Override
            public void callback(String url, NegotationItemResponse item, AjaxStatus status) {
                super.callback(url, result, status);
                //WeSwopLog("Status code " + status.getCode());

                if (status.getCode() == 200) {
                    //vado al dettaglio trattativa
                    Intent i = new Intent(ItemDetailActivity.this, NegotiationActivity.class);
                    i.putExtra(WeSwopApplication.NEGOTIATION_DETAILS, result.response.id_negotiation);
                    startActivity(i);
                } else {
                    WeSwopApplication.toast("Errore nella creazione della trattativa");
                    WeSwopError("Error adding negotiation item" + status.getCode());
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
            }

        });
    }

    public interface OnDialogCallbackInterface {
        void onNewNegotiationClick();

        void onExistingNegotiationClick();

        void onPreferredClick();
    }
}
