package com.iacopomiorini.weswop.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.fragments.MainFragment;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.HomeRow;
import com.iacopomiorini.weswop.utilities.FastBlur;
import com.iacopomiorini.weswop.utilities.Util;
import com.iacopomiorini.weswop.views.CircleImageView;


/**
 * Created by iacopomiorini on 16/01/15.
 */
public class HomeRowsAdapter extends ArrayAdapter<HomeRow> {

    private Context context;
    private ImageLoader imageLoader;
    private int mRowHeight;
    private int resource;
    private int blurLevel = 10;
    private MainFragment.OnFragmentInteractionListener mListener;

    public HomeRowsAdapter(Context context, int resource, HomeRow[] objects, ImageLoader imageLoader, MainFragment.OnFragmentInteractionListener mListener) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.imageLoader=imageLoader;
        this.mListener=mListener;
        mRowHeight = Util.getScreenWidth(context)/2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final HomeRowHolder holder;

        final HomeRow rowData = getItem(position);

        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(resource, parent, false);

            holder = new HomeRowHolder();

            holder.mainLayout=(LinearLayout)rowView.findViewById(R.id.home_row_main);
            holder.rowCircleImageView=(CircleImageView)rowView.findViewById(R.id.imageview_home_row_circleimageview);
            holder.backgroudNetworkImageView=(ImageView)rowView.findViewById(R.id.backgroud_network_image_view);
            holder.rowNotifica=(TextView)rowView.findViewById(R.id.textview_home_row_notifica);
            holder.rowTitle=(TextView)rowView.findViewById(R.id.textview_home_row_title);
            holder.rowUltimo=(TextView)rowView.findViewById(R.id.textview_home_row_ultimo);
            holder.rowData=(TextView)rowView.findViewById(R.id.textview_home_row_data);
            holder.rowOra=(TextView)rowView.findViewById(R.id.textview_home_row_ora);

            rowView.setTag(holder);
        }else{
            holder = (HomeRowHolder)rowView.getTag();
        }

        ImageRequest ir = new ImageRequest(rowData.imgUrl, new Response.Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                //makes bitmap mutable for blurring process
                response = response.copy(response.getConfig(), true);
                holder.backgroudNetworkImageView.setImageBitmap(FastBlur.doBlur(response, blurLevel, true));
            }
        }, mRowHeight, mRowHeight, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        } );

        WeSwopApplication.getInstance().addToRequestQueue(ir, "Row Image Request");

        holder.rowCircleImageView.setImageUrl(rowData.imgUrl, imageLoader);
        holder.rowTitle.setText(rowData.title);
        holder.rowNotifica.setText(Integer.toString(rowData.notification));
        holder.rowUltimo.setText(Util.capitalize(context.getResources().getString(R.string.last_message_from) + " " + rowData.description));
        holder.rowData.setText(rowData.data);
        holder.rowOra.setText(rowData.hour);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(rowData.id);
            }
        });

        ViewGroup.LayoutParams params = rowView.getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mRowHeight);
        } else {
            params.height = mRowHeight;
        }
        rowView.setLayoutParams(params);

        return rowView;
    }

    static class HomeRowHolder
    {
        LinearLayout mainLayout;
        CircleImageView rowCircleImageView;
        ImageView backgroudNetworkImageView;
        TextView rowNotifica;
        TextView rowTitle;
        TextView rowUltimo;
        TextView rowData;
        TextView rowOra;
    }


}
