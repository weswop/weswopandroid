package com.iacopomiorini.weswop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Item;
import com.iacopomiorini.weswop.views.CircleImageView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by iacopomiorini on 20/09/15.
 */
public class ItemsAdapter extends BaseAdapter {

        private ArrayList<Item> mData = new ArrayList<>();
        private LayoutInflater mInflater;
        private ImageLoader imageLoader;
        private Context context;

        public ItemsAdapter(Item[] data, Context context) {
            this.context=context;
            mInflater = (LayoutInflater) WeSwopApplication.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mData = new ArrayList<>(Arrays.asList(data));
            imageLoader = WeSwopApplication.getInstance().getImageLoader();
        }

        public void addItem(final Item item) {
            mData.add(item);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Item getItem(int position) {
            return mData.get(position);
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.explore_row, null);
                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.description = (TextView) convertView.findViewById(R.id.description);
                holder.price = (TextView) convertView.findViewById(R.id.price);
                holder.circleImageView = (CircleImageView) convertView.findViewById(android.R.id.icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.title.setText(mData.get(position).title);
            String descr = mData.get(position).descr;
            if (descr.length() > 60) {
                descr = descr.substring(0, 60);
            }
            holder.description.setText(descr);
            holder.price.setText(getItem(position).value+"€");
            if (getItem(position).images.length > 0) {
                String rootPath = context.getResources().getString(R.string.ROOT_PATH);
                String urlImg = String.format(rootPath, getItem(position).images[0].image_path);
                holder.circleImageView.setImageUrl(urlImg, imageLoader);
            }else{
                holder.circleImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder));
            }
            return convertView;
        }

    public static class ViewHolder {
        public TextView title;
        public TextView description;
        public TextView price;
        public CircleImageView circleImageView;
    }
}
