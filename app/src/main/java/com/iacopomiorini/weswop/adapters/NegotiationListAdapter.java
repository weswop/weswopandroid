package com.iacopomiorini.weswop.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.NegotiationDetail;
import com.iacopomiorini.weswop.models.UserInfo;
import com.iacopomiorini.weswop.views.CircleImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by iacopomiorini on 17/09/15.
 */
public class NegotiationListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Map<String, List<NegotiationDetail>> negotiationCollections;
    private List<String> negotiationCategories;

    private NegotiationDetail[] negotiations;

    /*
        Tipi Trattative per accordion:
            - In attesa di risposta
            - In corso
            - Non ancora avviate
            - Concluse ultimi 30 gg
            - Rifiutate ultimi 30 gg
     */

    public NegotiationListAdapter(Context context, List<String> negotiationCategories,
                                 Map<String, List<NegotiationDetail>> negotiationCollections) {
        this.context = context;
        this.negotiationCollections = negotiationCollections;
        this.negotiationCategories = negotiationCategories;
    }
    public NegotiationListAdapter(Context context, NegotiationDetail[] negotiations) {
        this.context = context;
        this.negotiations = negotiations;
        negotiationCollections = new HashMap<String, List<NegotiationDetail>>();
        negotiationCollections.put("Tutte", new ArrayList<>(Arrays.asList(negotiations)));
        negotiationCategories = new ArrayList<String>();
        negotiationCategories.add("Tutte");
    }

    public Object getChild(int groupPosition, int childPosition) {
        return negotiationCollections.get(negotiationCategories.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final NegotiationDetail negotiation = (NegotiationDetail) getChild(groupPosition, childPosition);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.negotiation_item, null);
        }

        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();

        Boolean imVendor=false;
        if (userInfo.id.equals(negotiation.id_vendor)){
            imVendor=true;
        }

        TextView startDate = (TextView) convertView.findViewById(R.id.tv_negotiation_row_started);
        CircleImageView circleImageViewLeft = (CircleImageView)convertView.findViewById(R.id.circleimageview_negotiation_row_left);
        CircleImageView circleImageViewRight = (CircleImageView)convertView.findViewById(R.id.circleimageview_negotiation_row_right);
        FrameLayout bgLeft = (FrameLayout)convertView.findViewById(R.id.bg_negotiation_row_balance_left);
        FrameLayout bgRight = (FrameLayout)convertView.findViewById(R.id.bg_negotiation_row_balance_right);
        TextView sumLeft = (TextView)convertView.findViewById(R.id.tv_negotiation_row_balance_objects_left);
        TextView sumRight = (TextView)convertView.findViewById(R.id.tv_negotiation_row_balance_objects_right);

        String urlImgLeft ="";
        if (imVendor){
            urlImgLeft =  new String("https://graph.facebook.com/"+negotiation.id_buyer+"/picture?type=large");
        }else{
            urlImgLeft =  new String("https://graph.facebook.com/"+negotiation.id_vendor+"/picture?type=large");
        }
        String urlImgRight = new String("https://graph.facebook.com/"+userInfo.id+"/picture?type=large");


        circleImageViewLeft.setImageUrl(urlImgLeft, WeSwopApplication.getInstance().getImageLoader());
        circleImageViewRight.setImageUrl(urlImgRight, WeSwopApplication.getInstance().getImageLoader());

        startDate.setText(negotiation.created);
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return negotiationCollections.get(negotiationCategories.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return negotiationCategories.get(groupPosition);
    }

    public int getGroupCount() {
        return negotiationCategories.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String negotiationGroup = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.negotiation_group,
                    null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.negotiation_group);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(negotiationGroup);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
