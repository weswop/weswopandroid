package com.iacopomiorini.weswop.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.WeSwopImage;

import java.util.List;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

/**
 * Created by iacopomiorini on 21/01/15.
 */
public class ImagePagerAdapter extends PagerAdapter {
    private Context context;
    private WeSwopImage[] images;
    private ImageLoader imageLoader;

    public ImagePagerAdapter(Context context, WeSwopImage[] images, ImageLoader imageLoader) {
        this.context = context;
        this.images = images;
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_object_detail_pager, null);
        NetworkImageView image = (NetworkImageView)view.findViewById(R.id.objectDetail_pager_image);
        String rootPath = context.getResources().getString(R.string.ROOT_PATH);
        String urlImg = String.format(rootPath, images[position].image_path);
        //WeSwopLog("Image Url "+urlImg);
        image.setImageUrl(urlImg, imageLoader);
        ((ViewPager) container).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}