package com.iacopomiorini.weswop.helpers;

import android.util.Log;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.Transformer;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.iacopomiorini.weswop.managers.WeSwopApplication;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopServiceLog;


/**
 * Created by iacopomiorini on 21/01/15.
 */
public class GSONHelper {
    private Gson gson;
    private static final GSONHelper instance;
    private static final GsonTransformer transformer;

    static {
        instance = new GSONHelper();
        transformer = instance.new GsonTransformer();
    }

    private GSONHelper() {
        gson = new GsonBuilder().serializeNulls().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).registerTypeAdapter(Date.class, new DateDeserializer()).create();
    }

    public static synchronized GSONHelper getInstance() {
        return instance;
    }

    public <T> T decode(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

    public String encode(Object object) {
        return gson.toJson(object);
    }

    public Transformer getTransformer() {
        return transformer;
    }

    public class GsonTransformer implements Transformer {

        public <T> T transform(String url, Class<T> type, String encoding, byte[] data, AjaxStatus status) {

            WeSwopServiceLog("transforming " + url);

			/*
			 * try{ Thread.sleep(500); } catch (Exception e){}
			 */

            WeSwopServiceLog("response\n" + new String(data));

            T object = null;
            try {
                object = gson.fromJson(new String(data), type);
                WeSwopServiceLog("transformed " + url + " to " + encode(object));
            } catch (Throwable t) {
                Log.e(WeSwopApplication.TAG, t.getMessage(), t);
            }

            return object;
        }
    }

    private static class DateDeserializer implements JsonDeserializer<Date> {

        private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            try {
                return df.parse(json.getAsString());
            } catch (ParseException e) {
                Log.w(WeSwopApplication.TAG, e.getMessage());
                return null;
            }
        }
    }

}
