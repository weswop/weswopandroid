package com.iacopomiorini.weswop.helpers;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.HomeRow;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

/**
 * Created by iacopomiorini on 15/01/15.
 */
public class HomeRequest<T> extends JsonRequest<T> {

    Gson gson = new Gson();

    public HomeRequest(Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Request.Method.GET, WeSwopApplication.URL_HOME_JSON, null, listener, errorListener);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            String jsonString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
            JsonObject object = gson.fromJson(jsonString, JsonElement.class).getAsJsonObject();
            if (object.get("status").toString().equals("\"success\"")){
                Type collectionType = new TypeToken<HomeRow[]>(){}.getType();
                T response = gson.fromJson(object.get("data").toString(), collectionType);
                Response<T> result = Response.success(response,
                        HttpHeaderParser.parseCacheHeaders(networkResponse));
                return result;
            }else{
                //error dialog
                return null;
            }
        } catch (UnsupportedEncodingException e) {
            return com.android.volley.Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return com.android.volley.Response.error(new ParseError(e));
        }
    }

}
