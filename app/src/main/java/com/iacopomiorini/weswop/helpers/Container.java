package com.iacopomiorini.weswop.helpers;

import java.io.Serializable;

/**
 * Created by iacopomiorini on 26/01/15.
 */
public class Container<T extends Serializable>  implements Serializable{
    private T content;

    public Container(T content) {
        this.content= content;
    }

    public T getContent() {
        return content;
    }
}
