package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iacopomiorini.weswop.R;

import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.CategoriesResponse;
import com.iacopomiorini.weswop.models.Category;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class CategoriesFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private AbsListView mListView, mListViewSubcategories;
    private SubcategoriesAdapter subcategoriesAdapter;
    private CategoriesAdapter categoriesAdapter;
    private Category[] categoriesList;
    private Category.Subcategory[] subcategoriesList;
    private boolean isSubcategoriesList = false;
    private String selected_id_cat;

    public static CategoriesFragment newInstance(String param1, String param2) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CategoriesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_categories;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        categoriesList = WeSwopApplication.getInstance().getCategories();
        categoriesAdapter = new CategoriesAdapter(getActivity(), android.R.layout.simple_list_item_1, categoriesList);
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListViewSubcategories = (AbsListView) view.findViewById(R.id.list_subcategory);
        mListView.setAdapter(categoriesAdapter);
        mListView.setOnItemClickListener(this);

        subcategoriesList = new Category.Subcategory[0];
        subcategoriesAdapter = new SubcategoriesAdapter(getActivity(), android.R.layout.simple_list_item_1, subcategoriesList);
        mListViewSubcategories.setAdapter(subcategoriesAdapter);
        mListViewSubcategories.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {

            if (isSubcategoriesList) {
                mListener.onFragmentInteraction(selected_id_cat + "_" + subcategoriesList[position].id_subcat);
            } else {
                isSubcategoriesList = true;
                selected_id_cat = categoriesList[position].id_cat;
                subcategoriesList = categoriesList[position].subcategories;
                subcategoriesAdapter.updateResults(categoriesList[position].subcategories);
                //WeSwopLog("Selected category "+ categoriesList[position].name+" -> "+ categoriesList[position].subcategories.length);
                mListView.setVisibility(View.GONE);
            }
        }
    }

    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    private class CategoriesAdapter extends BaseAdapter {

        Context context;
        int layoutResourceId;
        Category categories[] = null;

        public CategoriesAdapter(Context context, int layoutResourceId, Category[] data) {
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.categories = data;
        }

        @Override
        public int getCount() {
            return categories.length;
        }

        @Override
        public Object getItem(int position) {
            return categories[position];
        }

        @Override
        public long getItemId(int position) {
            return Integer.parseInt(categories[position].id_cat);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            CategoriesHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new CategoriesHolder();
                holder.txtTitle = (TextView) row.findViewById(android.R.id.text1);

                row.setTag(holder);
            } else {
                holder = (CategoriesHolder) row.getTag();
            }

            Category category = categories[position];
            holder.txtTitle.setText(category.name);

            return row;
        }

        class CategoriesHolder {
            TextView txtTitle;
        }
    }

    private class SubcategoriesAdapter extends BaseAdapter {

        Context context;
        int layoutResourceId;
        Category.Subcategory subcategories[] = null;

        public SubcategoriesAdapter(Context context, int layoutResourceId, Category.Subcategory[] data) {
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.subcategories = data;
        }

        @Override
        public int getCount() {
            return this.subcategories.length;
        }

        @Override
        public Object getItem(int position) {
            return this.subcategories[position];
        }

        @Override
        public long getItemId(int position) {
            return Integer.parseInt(this.subcategories[position].id_subcat);
        }

        public void updateResults(Category.Subcategory[] results) {
            this.subcategories = new Category.Subcategory[0];
            this.subcategories = results;
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            CategoriesHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new CategoriesHolder();
                holder.txtTitle = (TextView) row.findViewById(android.R.id.text1);

                row.setTag(holder);
            } else {
                holder = (CategoriesHolder) row.getTag();
            }

            Category.Subcategory subcategory = subcategories[position];
            holder.txtTitle.setText(subcategory.name);

            return row;
        }

        class CategoriesHolder {
            TextView txtTitle;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (isSubcategoriesList) {
                        mListView.setVisibility(View.VISIBLE);
                        isSubcategoriesList = false;
                    } else {

                    }
                    return true;
                }
                return false;
            }
        });
    }
}