package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.managers.WeSwopApplication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class FacebookFragment extends Fragment {
    private static final String TAG = "FacebookFragment";
    private UiLifecycleHelper uiHelper;
    private onLoginFacebookListener mListener;
    private LoginButton authButton;
    private boolean logged=false;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facebook, container, false);

        authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setFragment(this);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        WeSwopLog("state "+state);
        if (state.isOpened()) {
            Log.i(TAG, "Logged in (FB)...");
            WeSwopLog("Change status open");
            if (!logged){
                authButton.setVisibility(View.GONE);
            }
            logged=true;
            mListener.onLogin(session);
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out (FB)...");
            WeSwopLog("Change status closed");
            if (logged){
                authButton.setVisibility(View.VISIBLE);
            }
            logged=false;
            mListener.onLogout();

        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onLoginFacebookListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public interface onLoginFacebookListener {
        public void onLogin(Session session);
        public void onLogout();
    }
}
