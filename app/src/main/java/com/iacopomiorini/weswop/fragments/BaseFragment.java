package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.managers.WeSwopApplication;

/**
 * Created by iacopomiorini on 19/12/14.
 */
public abstract class BaseFragment extends Fragment {

    protected abstract int getLayoutResId();

    protected RequestQueue mRequestQueue;
    protected static ImageLoader mImageLoader;
    protected ProgressDialog pleaseWait;
    protected AQuery $;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResId(), container, false);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        mRequestQueue = Volley.newRequestQueue(activity.getBaseContext());
        mImageLoader = WeSwopApplication.getInstance().getImageLoader();

        $ = new AQuery(activity.getBaseContext());

        pleaseWait = new ProgressDialog(activity);
        pleaseWait.setIndeterminate(true);
        pleaseWait.setCancelable(false);
        pleaseWait.setMessage(getString(R.string.loading));

        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
