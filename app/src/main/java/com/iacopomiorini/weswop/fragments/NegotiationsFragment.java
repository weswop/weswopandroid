package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.adapters.NegotiationListAdapter;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.NegotationsResponse;
import com.iacopomiorini.weswop.models.NegotiationDetail;
import com.iacopomiorini.weswop.models.UserInfo;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopError;
import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class NegotiationsFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private ExpandableListView elv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NegotiationDetail[] negotiations;
    private UserInfo userInfo;
    private String strGetNegotiations,urlGetNegotiations;

    public NegotiationsFragment() {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_trattative;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        elv = (ExpandableListView) root.findViewById(android.R.id.list);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.negotiation_swipe_refresh_layout);

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNegotiationData();
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (negotiations == null){
            getNegotiationData();
        }else{
            setUpView();
        }
    }

    private void getNegotiationData(){

        userInfo = WeSwopApplication.getInstance().getUserInfo();
        if (userInfo!=null){
            strGetNegotiations = getResources().getString(R.string.URL_GET_NEGOTIATION_USER);
            urlGetNegotiations = String.format(strGetNegotiations, userInfo.id, userInfo.wwToken);
            swipeRefreshLayout.setRefreshing(true);

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlGetNegotiations, NegotationsResponse.class, new AjaxCallback<NegotationsResponse>() {
            @Override
            public void callback(String url, final NegotationsResponse result, AjaxStatus status) {
                super.callback(url, result, status);
                swipeRefreshLayout.setRefreshing(false);

                if (status.getCode() == 200) {
                    negotiations = result.response;
                    setUpView();
                } else {
                    WeSwopApplication.toast("Errore di connessione al server");
                    WeSwopError("Error get negotiations "+status.getCode());
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                WeSwopApplication.toast("Errore di connessione al server");
                WeSwopError("Error get negotiations "+code+" "+message);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        }

    }

    private void setUpView(){
        elv.setAdapter(new NegotiationListAdapter(getActivity().getApplicationContext(), negotiations));
        elv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                WeSwopLog("Clicked negotiation "+negotiations[childPosition].id_negotiation);
                if (mListener != null) {
                    WeSwopLog("Clicked negotiation 2 "+negotiations[childPosition].id_negotiation);
                    mListener.openNegotiationDetail(negotiations[childPosition]);
                }
                return false;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void openNegotiationDetail(NegotiationDetail negotiation);
    }

}
