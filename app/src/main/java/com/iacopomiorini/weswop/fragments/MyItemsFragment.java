package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.adapters.ItemsAdapter;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Item;
import com.iacopomiorini.weswop.models.ItemsResponse;
import com.iacopomiorini.weswop.models.UserInfo;
import com.iacopomiorini.weswop.views.CustomDialog;

public class MyItemsFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private ListView lv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Item[] items;

    public static MyItemsFragment newInstance(String param1, String param2) {
        MyItemsFragment fragment = new MyItemsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public MyItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_oggetti;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        lv = (ListView) root.findViewById(android.R.id.list);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.my_items_swipe_refresh_layout);

        root.findViewById(R.id.fab_add_object).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog dialog = new CustomDialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
                dialog.setNewItemMenu();
                dialog.show();
            }
        });

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyItemsData();
            }
        });

        if (items == null) {
            getMyItemsData();
        } else {
            setUpView();
        }

        return root;
    }

    public void onButtonPressed(Item item) {
        if (mListener != null) {
            mListener.openObjectDetail(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void openObjectDetail(Item item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void getMyItemsData() {

        swipeRefreshLayout.setRefreshing(true);

        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();
        String strAllItems = getResources().getString(R.string.URL_GET_USER_ITEMS);
        String urlAllItems = String.format(strAllItems, userInfo.id, userInfo.wwToken);

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlAllItems, ItemsResponse.class, new AjaxCallback<ItemsResponse>() {
            @Override
            public void callback(String url, final ItemsResponse result, AjaxStatus status) {
                super.callback(url, result, status);
                swipeRefreshLayout.setRefreshing(false);

                if (status.getCode() == 200) {
                    items = result.response;
                    setUpView();
                } else {

                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void setUpView() {
        lv.setAdapter(new ItemsAdapter(items, getActivity().getApplicationContext()));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.openObjectDetail(items[position]);
                }
            }
        });
    }


}
