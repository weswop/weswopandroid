package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.adapters.ItemsAdapter;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.managers.WeSwopApplication;
import com.iacopomiorini.weswop.models.Item;
import com.iacopomiorini.weswop.models.ItemResponse;
import com.iacopomiorini.weswop.models.ItemsResponse;
import com.iacopomiorini.weswop.models.UserInfo;

import java.util.HashMap;
import java.util.Map;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopError;
import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class ExploreFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private ListView lv, lvAdvanced;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout advancedSearchLayout;
    private Item[] items;
    private Button searchAllItemsButton, advancedSearchButton;
    private ProgressDialog dialog;

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_esplora;
    }

    public static ExploreFragment newInstance() {
        ExploreFragment fragment = new ExploreFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.loading);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        lv = (ListView) root.findViewById(android.R.id.list);
        lvAdvanced = (ListView) root.findViewById(R.id.lv_explore_advanced_search);
        advancedSearchLayout = (LinearLayout)root.findViewById(R.id.ll_advanced_search);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.explore_swipe_refresh_layout);
        searchAllItemsButton = (Button) root.findViewById(R.id.button_explore_all_items);
        advancedSearchButton = (Button) root.findViewById(R.id.button_explore_advanced_search);

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getExploreData(true);
            }
        });

        if (items == null) {
            getExploreData(true);
        } else {
            setUpView();
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void getExploreData(boolean refresh) {

        swipeRefreshLayout.setRefreshing(true);

        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();
        String strAllItems = getResources().getString(R.string.URL_GET_ALL_ITEMS);
        String urlAllItems = String.format(strAllItems, userInfo.id, userInfo.wwToken);

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlAllItems, ItemsResponse.class, new AjaxCallback<ItemsResponse>() {
            @Override
            public void callback(String url, final ItemsResponse result, AjaxStatus status) {
                super.callback(url, result, status);
                swipeRefreshLayout.setRefreshing(false);

                if (status.getCode() == 200) {
                    items = result.response;
                    setUpView();
                } else {

                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void makeAdvancedSearch(){
        UserInfo userInfo = WeSwopApplication.getInstance().getUserInfo();

        String urlAdvancedSearch = getResources().getString(R.string.URL_SEARCH_ITEM);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id_fb", userInfo.id);
        params.put("wwtoken", userInfo.fbToken);
        params.put("id_cat", "");
        params.put("id_subcat", "");
        params.put("type", "");
        params.put("keywords", "");
        params.put("value_min", "");
        params.put("value_max", "");
        params.put("search_lat search_lon", "");
        params.put("range", "");
        params.put("prov", "");
        params.put("postcode", "");
        params.put("city", "");
        params.put("limit1", "");
        params.put("limit2", "");
        params.put("lang", "");

        $.progress(dialog).ajax(urlAdvancedSearch, params, ItemResponse.class, new AjaxCallback<ItemResponse>() {

            @Override
            public void callback(String url, ItemResponse response, AjaxStatus status) {
                super.callback(url, result, status);
                WeSwopLog("Posting image for object " + response.response);
                if (status.getCode() == 200) {
                    WeSwopLog("Upload success photo!");
                } else {
                    WeSwopApplication.toast("Upload photo error");
                    WeSwopError("Upload photo error " + this.status.getCode());
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                WeSwopApplication.toast("New item created but upload photo error");
                WeSwopError("Upload photo error " + code + " " + message);
            }
        });

    }

    private void setUpView() {
        lv.setAdapter(new ItemsAdapter(items, getActivity().getApplicationContext()));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.openObjectDetail(items[position]);
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void openObjectDetail(Item item);
    }

}
