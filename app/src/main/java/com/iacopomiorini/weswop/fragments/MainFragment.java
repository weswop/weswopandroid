package com.iacopomiorini.weswop.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.iacopomiorini.weswop.R;
import com.iacopomiorini.weswop.adapters.HomeRowsAdapter;
import com.iacopomiorini.weswop.helpers.GSONHelper;
import com.iacopomiorini.weswop.models.HomeRow;
import com.iacopomiorini.weswop.models.HomeRowResponse;

import static com.iacopomiorini.weswop.managers.WeSwopApplication.WeSwopLog;

public class MainFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private HomeRow[] homeRowsData;
    SwipeRefreshLayout homeSwipeRefreshLayout;
    ListView homeListView;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    public MainFragment() {
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        homeSwipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.home_swipe_refresh_layout);
        homeListView = (ListView) getView().findViewById(R.id.home_listview);

        homeSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        homeSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHomeData(true);
            }
        });

        if (homeRowsData != null) {
            populateListView(homeRowsData);
        } else {
            getHomeData(false);
        }
    }

    private void getHomeData(final boolean refresh) {
        /*
        homeSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                homeSwipeRefreshLayout.setRefreshing(true);
            }
        });
        */
        if (refresh) {
            homeSwipeRefreshLayout.setRefreshing(true);
        } else {
            pleaseWait.show();
        }

        String urlHome = getResources().getString(R.string.URL_HOME_JSON);

        $.transformer(GSONHelper.getInstance().getTransformer()).ajax(urlHome, HomeRowResponse.class, new AjaxCallback<HomeRowResponse>() {
            @Override
            public void callback(String url, HomeRowResponse result, AjaxStatus status) {
                super.callback(url, result, status);

                if (status.getCode() == 200) {
                    if (result.response != null) {
                        homeRowsData = result.response;
                        populateListView(result.response);
                        if (refresh) {
                            homeSwipeRefreshLayout.setRefreshing(false);
                        } else if (pleaseWait.isShowing()) {
                            pleaseWait.dismiss();
                        }
                    }
                }
            }

            @Override
            public void failure(int code, String message) {
                super.failure(code, message);
                if (refresh) {
                    homeSwipeRefreshLayout.setRefreshing(false);
                } else if (pleaseWait.isShowing()) {
                    pleaseWait.dismiss();
                }
            }
        });
    }

    private void populateListView(HomeRow[] homeRowsData) {
        HomeRowsAdapter hra = new HomeRowsAdapter(this.getActivity().getBaseContext(), R.layout.home_row_layout, homeRowsData, mImageLoader, mListener);
        homeListView.setAdapter(hra);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putSerializable("home_data", (Serializable) homeRowsData);
    }

    /*
    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            //probably orientation change
            homeRowsData = (HomeRow[]) savedInstanceState.getSerializable("home_data");
        } else {
            if (homeRowsData != null) {
                //returning from backstack, data is fine, do nothing
            } else {
                //newly created, compute data
               getHomeData();
            }
        }
    }
    */

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(int page);
    }

}
