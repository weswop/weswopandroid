package com.iacopomiorini.weswop.managers;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.androidquery.callback.BitmapAjaxCallback;
import com.iacopomiorini.weswop.helpers.LruBitmapCache;
import com.iacopomiorini.weswop.models.Category;
import com.iacopomiorini.weswop.models.UserInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by iacopomiorini on 25/12/14.
 */
public class WeSwopApplication extends Application {

    public static final Map<Integer, Integer> idsPages;
    public static final String OBJECT_DETAILS = "object_details";
    public static final String NEGOTIATION_DETAILS = "negotiation_details";
    public static final String URL_HOME_JSON = "http://www.weswop.altervista.org/res/home.json";
    public static final String NEW_OBJECT_MODE = "new_object_mode";
    public static final String NEW_OBJECT_MODE_DESCRIPTION = "new_object_mode_description";
    public static final String NEW_OBJECT_MODE_PHOTO = "new_object_mode_photo";
    public static final String NEW_OBJECT_MODE_LOCATION = "new_object_mode_location";
    public static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 0;
    public static final int PLACE_PICKER_REQUEST = 1;
    public static final int REQUEST_CODE_CATEGORY = 2;
    public static final String TAG = WeSwopApplication.class.getSimpleName();
    public static final String TAG_GENERIC = WeSwopApplication.class.getSimpleName() + "_GENERIC";
    public static final String TAG_SERVICE = WeSwopApplication.class.getSimpleName() + "_SERVICE";
    public static final int PAGE_HOME = 0;
    public static final int PAGE_ESPLORA = 1;
    public static final int PAGE_TRATTATIVE = 2;
    public static final int PAGE_MIEI_OGGETTI = 3;
    public static final int PAGE_NOTIFICHE_MESSAGGI = 4;
    public static final int PAGE_TEST = 5;
    public static final int PAGES_NUMBER = 5;
    private static WeSwopApplication mInstance;
    private static Toast toast;

    static {
        idsPages = new HashMap<>();
        idsPages.put(1, WeSwopApplication.PAGE_NOTIFICHE_MESSAGGI);
        idsPages.put(2, WeSwopApplication.PAGE_TRATTATIVE);
        idsPages.put(3, WeSwopApplication.PAGE_MIEI_OGGETTI);
        idsPages.put(4, WeSwopApplication.PAGE_ESPLORA);
        idsPages.put(5, WeSwopApplication.PAGE_ESPLORA);
        //idsPages = Collections.unmodifiableMap(idsPages);
    }

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private UserInfo userInfo;
    private Category[] categories;

    public static synchronized WeSwopApplication getInstance() {
        return mInstance;
    }

    public static void WeSwopLog(String string) {
        Log.d(TAG_GENERIC, string);
    }

    public static void WeSwopError(String string) {
        Log.e(TAG_GENERIC, string);
    }

    public static void WeSwopLog(int i) {
        WeSwopLog(String.valueOf(i));
    }

    public static void WeSwopError(int i) {
        WeSwopError(String.valueOf(i));
    }

    public static void WeSwopServiceLog(String string) {
        Log.d(TAG_SERVICE, string);
    }

    public static void toast(String message) {
        WeSwopApplication.toast(message, Toast.LENGTH_SHORT);
    }

    public static void toast(String message, int duration) {
        WeSwopApplication.toast.makeText(WeSwopApplication.getInstance(), message, duration).show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    public Category getCategoryById(int categoryId) {
        for (Category category : categories) {
            WeSwopLog(category.subcategories[0].id_cat + " - " + categoryId);
            if (category.id_cat.equals(categoryId)) {
                return category;
            }
        }
        return null;
    }

    public Category getCategorySubcategoryById(String categoryId, String subcategoryId) {
        Category retCategory = new Category();
        Category.Subcategory retSubcat = null;

        WeSwopLog("Finding cat, subcat for " + categoryId + ", " + subcategoryId);
        for (Category category : categories) {
            if (category.subcategories.length > 0) {
                WeSwopLog(category.subcategories[0].id_cat + " - " + categoryId);
            }
            if (category.id_cat.equals(categoryId)) {
                retCategory.id_cat = categoryId;
                retCategory.name = category.name;
                for (Category.Subcategory subcategory : category.subcategories) {
                    if (subcategory.id_subcat.equals(subcategoryId)) {
                        retSubcat = subcategory;
                    }
                }
                retCategory.subcategories = new Category.Subcategory[1];
                retCategory.subcategories[0] = retSubcat;
                WeSwopLog("Find " + retCategory.subcategories[0].name + " inside " + retCategory.name);
                return retCategory;
            }
        }
        return null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        BitmapAjaxCallback.clearCache();
    }

}
